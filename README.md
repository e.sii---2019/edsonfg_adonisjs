# Aplicação para demostração de algumas funcionalidades do framework AdonisJs

### Pré-requisitos 

*  Para criar um projeto em AdonisJs é exigido duas dependências: NodeJs versão 8.0.0 ou superior e npm versão 3.0.0 ou superior.

### Tecnologias Utilizadas 

*  NodeJs 8.0.0
*  npm 3.0.0
*  AdonisJs 4.0.12
*  Apache 
*  Mysql
*  Javascrip
*  Html

### Licença Utilizada

*  Mit License
*  Link para licença: https://gitlab.com/e.sii---2019/edsonfg_adonisjs/blob/master/LICENSE

#### Como rodar a aplicação passo a passo

*  Baixe e instale na sua máquina os Softwares: Xampp e Nodejs (A instalação destes softwares é simples como qualquer outro);
*  Se não tiver na sua máquina baixe e instale um editor de texto (recomendo o **VSCode**, na minha minha opinião um dos melhores gratuitos disponíveis e a instalação também é simples);
*  Abra o xampp e clique no **start** em Apache e Mysql.
*  No browser entre no servidor em **localhost/phpmyadmin** e crie um novo banco de dados chamado **resale**.
*  Abra o editor de texto.
*  Pressione **control** e **apóstrofo** para abrir o terminal direto pelo editor (você pode fazer isso pelo cmd do windowns se preferir).
*  Digite o comando **npm i -g @adonisjs/cli** para instalar o Adonis globalmente.
*  Abra o projeto baixado.
*  No terminal digite o comando **adonis migration:run** para rodar as migrações para o banco de dados criado.
*  Digite **adonis serve --dev** para startar o projeto
*  No browser digite **localhost:3333/form** para entrar na view e fazer as insersões.

Para testar o método destroy da controller(delete) voçê vai precisar baixar e instalar o software **Postman**. Nele através da URL **localhost:3333/all_cars** voçê pode listar o dados ou deletá-los.

#### Video explicativo  

*  Link do Youtube => https://youtu.be/idCHgQWvhSQ

